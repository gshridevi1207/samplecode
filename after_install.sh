#!/bin/bash
Home=/var/www/html
LOG=$HOME/log/deploy.log

 /bin/echo "$(date '+%y-%m-%d'): ** Before Install Hook started **">> $LOG
 /bin/echo "$(date '+%y-%m-%d'): changing owner and group of application..">> $LOG
 
 /usr/bin/sudo /bin/chown -R ec2-user:ec2-user $HOME
 
 echo -e "Done" >> $LOG
 
 /bin/echo "$(date '+%y-%m-%d' %x'): **After Install Hook Completed **>> $LOG